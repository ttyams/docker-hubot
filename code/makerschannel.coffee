# Description:
#   Allows hubot to run commands on Makerschannels API.
#
# Dependencies:
#   It's all in the docker image
#
# Configuration:
#   Don't worry about it ;)
#
# Commands:
#   hubot likes from <number> week(s)/ month(s) ago - get the amount of likes within the queried week / month
#   hubot likes this week / month - get the amount of likes within the queried week / month
#   hubot follows previous week - get the amount of follows from previous week
#   hubot follows from <number> week(s) ago - get the amount of follows within the queried week / month
#   hubot most liked from <number> week(s)/ month(s) ago - get the most liked production within the queried week / month
#   hubot most liked this week/ month - get the most liked production within the queried week / month
#   hubot most active user - get the User that has the most likes and follows combined
#   hubot most active user for <number> week(s) ago - get the User that has the most likes and follows combined within the queried week
#   hubot most following user - get the User that has the most follows
#   hubot most liked user - get the User that has the most likes
#
# Author:
#   tomhoenderdos
#

http = require('http')
api_key = "pP8oKgUsFaU3=JaG}zfA+A@9x4pjJG.L8mbKGWKB7E;$M>HeFK"
hostname = "http://makerschannel.com"

module.exports = (robot) ->
  # Favorites Section
  robot.respond /likes from (\d+) (week|month)s? ago/, (res) ->
    week_amount = res.match[1]
    robot_response = res
    robot.http("#{hostname}/api/favorites/#{res.match[2]}s/#{week_amount}")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body
  robot.respond /likes this (week|month)s?/, (res) ->
    robot_response = res
    robot.http("#{hostname}/api/favorites/#{res.match[1]}s/0")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body

  # Following Section
  robot.respond /follows previous week/, (res) ->
    robot_response = res
    robot.http("#{hostname}/api/followings/weeks/1")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body
  robot.respond /follows from (\d+) weeks? ago/, (res) ->
    week_amount = res.match[1]
    robot_response = res
    robot.http("#{hostname}/api/followings/weeks/#{week_amount}")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body

  # Production Section
  robot.respond /most liked from (\d+) (week|month)s? ago/, (res) ->
    week_amount = res.match[1]
    robot_response = res
    robot.http("#{hostname}/api/videos/most_favorited/#{res.match[2]}s/#{week_amount}")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body

  robot.respond /most liked this (week|month)?/, (res) ->
    robot_response = res
    robot.http("#{hostname}/api/videos/most_favorited/#{res.match[1]}s/0")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body

  # User Section
  robot.respond /most active user/, (res) ->
    robot_response = res
    robot.http("#{hostname}/api/users/most_active")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body
  robot.respond /most following user/, (res) ->
    robot_response = res
    robot.http("#{hostname}/api/users/most_following")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body
  robot.respond /most liked user/, (res) ->
    robot_response = res
    robot.http("#{hostname}/api/users/most_favorited")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body
  robot.respond /most active user for (\d+) weeks? ago/, (res) ->
    robot_response = res
    robot.http("#{hostname}/api/users/most_active/weeks/#{res.match[1]}")
      .auth(api_key)
      .get() (err, res, body) ->
        robot_response.send body
