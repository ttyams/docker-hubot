# docker-hubot
Container for running hubot in docker including Makerschannel's custom code.

###Install docker and docker-machine
To be able to run docker and docker-machine go to:
> https://docs.docker.com/engine/installation/

And follow download / installation instructions for the so-called `Docker Toolbox` app for OSX/Windows.
If this is not available in the future, then install separate with any found instruction  through docker website / google!


###Connect to running docker on machine
Run one time:
```
docker-machine create master --driver generic --generic-ip-address=192.168.3.234
```
Then add to your `~/.bash_rc` (or `~/.whatever_rc` ):
```
eval $(docker-machine env master)
```

###Build Container
From inside this repository on your local machine run:
```
docker build --tag=mc:docker .
```

###Run Container
From inside this repository on your local machine run:
```
docker run --env-file ./ENV --name hubot mc:docker
```

You will probably get the following error:
```
docker: Error response from daemon: Conflict. The name "/hubot" is already in use by container 424e6b9b5801c953ee889e3071beba5ef2be674cb615373069575cd87867a614. You have to remove (or rename) that container to be able to reuse that name..
```

Just destroy the old hubot instance
```
docker rm hubot
```

and try again